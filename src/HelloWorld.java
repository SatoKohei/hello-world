import java.util.Scanner;

public class HelloWorld {
    public static void main(String[] args) {
        int a = 30, b = 45;
        if ((a >= 10) && (b % 2 == 0)) {
            if (a * b > 500) {
                System.out.println("Large number.");
            } else {
                System.out.println("Small number.");
            }
        }
    }
}
